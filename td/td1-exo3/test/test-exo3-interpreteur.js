// Tests pour l'interpréteur de l'exo 3
const exo3 = require("../exo3-interpreteur");
const assert = require("assert");
const exo1 = require("../ex1-1.json");

describe("Exo3 module", function () {
  let doc1 = { toto: 1 };
  let doc2 = { titi: 2 };
  let doc3 = { docs: [doc1, doc2] };
  let root = exo3.root;
  let ctx = exo3.ctx;

  describe("evalC", function () {
    let getToto = root.at("toto");
    let getDocs0 = root.at("docs").at(0);
    let getSubdocs = root.desc();

    it(" is available", function () {
      assert.equal(typeof exo3.evalC, "function");
    });

    describe("evaluates $", function () {
      it(" as an Array", function () {
        assert.equal(Array.isArray(exo3.evalC(doc1, doc2, root)), true);
      });
      it(" as the root doc", function () {
        assert.deepEqual(exo3.evalC(doc1, doc2, root), [doc1]);
      });
    });

    describe("evaluates @", function () {
      it(" as an Array", function () {
        assert.equal(Array.isArray(exo3.evalC(doc1, doc2, ctx)), true);
      });
      it(" as the context doc", function () {
        assert.deepEqual(exo3.evalC(doc1, doc2, ctx), [doc2]);
      });
    });

    describe("evaluates .", function () {
      it(" as an Array", function () {
        assert.equal(Array.isArray(exo3.evalC(doc1, doc2, getToto)), true);
        assert.equal(Array.isArray(exo3.evalC(doc2, doc3, getToto)), true);
      });
      it(" and get field when it exists", function () {
        assert.deepEqual(exo3.evalC(doc1, doc2, getToto), [1]);
      });
      it(" and get empty result when field does not exist", function () {
        assert.deepEqual(exo3.evalC(doc2, doc1, getToto), []);
      });
      it(" when field is a number in an array", function () {
        assert.deepEqual(exo3.evalC(doc3, doc2, getDocs0), [doc1]);
      });
    });

    describe("evaluates ..", function () {
      it(" and returns all sub documents", function () {
        let result = exo3.evalC(doc3, doc2, getSubdocs);
        assert.equal(result.length, 6);
        for (d of [doc3, doc3.docs, doc1, doc1.toto, doc2, doc2.titi]) {
          assert.notEqual(result.indexOf(d), -1);
        }
      });
    });
  });

  describe("evalCond", function () {
    let evalCond = exo3.evalCond;
    let cTrue = exo3.cTrue;
    let cFalse = exo3.cFalse;
    let and = exo3.and;
    let or = exo3.or;
    let not = exo3.not;
    let cmpV = exo3.cmpV;
    let cmpC = exo3.cmpC;
    let exists = exo3.exists;
    it(" is available", function () {
      assert.equal(typeof exo3.evalCond, "function");
    });
    it("evaluates true as true", function () {
      assert.equal(evalCond(doc1, doc2, cTrue), true);
    });
    it("evaluates false as false", function () {
      assert.equal(evalCond(doc1, doc1, cFalse), false);
    });
    it("evaluates and properly", function () {
      assert.equal(evalCond(doc1, doc1, and(cTrue, cFalse)), false);
    });
    it("evaluates or properly", function () {
      assert.equal(evalCond(doc1, doc1, or(cTrue, cFalse)), true);
    });
    it("evaluates not properly", function () {
      assert.equal(evalCond(doc1, doc1, not(cFalse)), true);
    });
    it("evaluates cmpV with == properly for match", function () {
      assert.equal(evalCond(doc1, doc2, cmpV("==", ctx.at("titi"), 2)), true);
    });
    it("evaluates cmpV with == properly when don't match", function () {
      assert.equal(evalCond(doc1, doc2, cmpV("==", ctx.at("titi"), 1)), false);
    });
    it("evaluates cmpV with == properly when not found", function () {
      assert.equal(evalCond(doc1, doc2, cmpV("==", ctx.at("toto"), 1)), false);
    });
    it("evaluates cmpC with == properly when some node match", function () {
      assert.equal(
        evalCond(
          doc3,
          doc2,
          cmpC("==", ctx.at("titi"), root.at("docs").children().children())
        ),
        true
      );
    });
    it("evaluates cmpC with == properly when no match found", function () {
      assert.equal(
        evalCond(doc1, doc2, cmpC("==", ctx.at("titi"), root.at("toto"))),
        false
      );
    });
  });
  describe("check with queries on doc from exercice 1", function () {
    // running $[0].nom
    it("$[0].nom", function () {
      assert.equal(exo3.evalC(exo1, exo1, root.at(0).at("nom")), "AC/DC");
    });
    it("$[1].albums[0].titre", function () {
      assert.equal(
        exo3.evalC(exo1, exo1, root.at(1).at("albums").at(0).at("titre")),
        "Tubular Bells"
      );
    });
    it('$[*].albums[*] ?(@.pistes[*].titre == "Back in Black") .annee', function () {
      assert.deepEqual(
        exo3.evalC(
          exo1,
          exo1,
          root
            .children()
            .at("albums")
            .children()
            .cond(
              exo3.cmpV(
                "==",
                ctx.at("pistes").children().at("titre"),
                "Back in Black"
              )
            )
            .at("annee")
        ),
        [1980]
      );
    });
    it("$[*].albums[*] ?(@.titre == @.pistes[*].titre) .titre", function () {
      let result = exo3.evalC(
        exo1,
        exo1,
        root
          .children()
          .at("albums")
          .children()
          .cond(
            exo3.cmpC(
              "==",
              ctx.at("titre"),
              ctx.at("pistes").children().at("titre")
            )
          )
          .at("titre")
      );
      assert.equal(result.length, 2);
      for (v of ["Highway to Hell", "Back in Black"]) {
        assert.notEqual(result.indexOf(v), -1);
      }
    });
  });
});
