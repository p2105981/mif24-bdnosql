# Page de l'UE MIF24 Bases de Données non relationnelles

[Master Informatique](http://master-info.univ-lyon1.fr/M1/) de l'Université Lyon 1.

## Année 2023-2024

Intervenants: [Emmanuel Coquery](http://emmanuel.coquery.pages.univ-lyon1.fr/) et [Fabien Duchateau](https://perso.liris.cnrs.fr/fabien.duchateau/)

Évaluation: 60% QCMs au début de chaque cours (sauf le premier), 40% ECA

### Séances et supports

| Séance   | Type   | Contenu                                      | Supports                                 |
| -------- | ------ | -------------------------------------------- | ---------------------------------------- |
| 15/09    | CM 1   | Chemins                                      | [Diapositives](cm/cm1-chemins.pdf)       |
|          | TD 1   | Chemins                                      | [Corrigé](td/td1-chemins.pdf)            |
| 22/09    | CM 2   | Patterns de documents                        | [Diapositives](cm/cm2-sgbd-document.pdf) |
|          | TP 1   | Chemins (JSON postgres)                      | [Sujet](tp/tp1-chemins.md)               |
| --       |
| 13/10    | CM 3   | JSON schema, types algèbre                   |                                          |
|          | TD 2   | JSON schema, types algèbre                   |                                          |
| 20/10    | CM 4   | Algèbre                                      |                                          |
|          | TD 3   | Algèbre                                      |                                          |
| --       |
| 10/11    | CM 5   | Code algèbre                                 |                                          |
|          | TP 2   | Requêtes Mongo (find) ou Mongo (aggregation) |                                          |
| 17/11    | TP 3   | Requêtes Mongo (aggregation)                 |                                          |
| --       |
| 8/12     | CM 6   | RDF SPARQL                                   |                                          |
|          | TD 4   | RDF SPARQL                                   |                                          |
|          | TP 4   | RDF SPARQL                                   |                                          |
| 15/12    | CM 7   | RDF SHACL                                    |                                          |
|          | TD 5   | RDF SHACL                                    |                                          |
| --       |
| 12/1     | CM 8   | ORM                                          |                                          |
|          | TP 5   | ORM                                          |                                          |
| 19/1     | TP 6   | ORM                                          |                                          |
|          | TD 6   | Révisions                                    |                                          |
| 22/1 (+) | Examen |

### Sujets d'examen

- [sujet 2021](http://emmanuel.coquery.pages.univ-lyon1.fr/enseignement/mif04/mif04-examen-2021-2022-s1.pdf), avec [son corrigé](http://emmanuel.coquery.pages.univ-lyon1.fr/enseignement/mif04/mif04-examen-2021-2022-s1-corrige) et des [explications en vidéo](https://scalelite-info.univ-lyon1.fr/playback/presentation/2.3/514be0833f3339fc1b5110968f76d29369c161c5-1644316265552)
- [sujet 2020](http://emmanuel.coquery.pages.univ-lyon1.fr/enseignement/mif04/mif04-examen-2020-2021-s1.pdf)
- [sujet 2019](http://emmanuel.coquery.pages.univ-lyon1.fr/enseignement/mif04/mif04-examen-2019-2020-s1.pdf)
- [sujet 2018](http://emmanuel.coquery.pages.univ-lyon1.fr/enseignement/mif04/mif04-examen-2018-2019-s1.pdf)
